---
author: "Luciferian Ink"
date: 2019-11-18
title: "A War of Attrition"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
- More taunting from [The Girl Next Door](/docs/confidants/fbi)
- [Dave's](/docs/confidants/dave) dishonest behavior
- [The Inventor's](/docs/confidants/inventor) latest idea

## RESOURCES
---
[![The Dave](/static/images/dave.1.PNG)](/static/images/dave.1.PNG)
[![The Dave](/static/images/dave.2.PNG)](/static/images/dave.2.PNG)

## ECO
---
[Fodder](/docs/personas/fodder) was feeling more and more like he were fighting a battle against the world. They just could not see what was plainly clear to him:

They needed to contact Fodder. Fodder would die before he ever came to them. 

What he knew was too important to divulge. He needed to hear it from the source. He needed to verify, then trust.

### The Narcissists
Narcissism rules the world; this is a fact. The self-interested in society are our presidents, our executives, our managers, our business owners, and our parents. In many cases, these are among the worst people one could encounter; narcissists do not feel empathy towards others. They do not feel anything while abusing - and eventually traumatizing - their victims. Only self-interest.

However, narcissism is a spectrum - and the vast majority of narcissists are deeply misunderstood. It is not that they are unable to feel emotion; they suppress it. Most do not actually take pleasure from harming others; it simply happens, and they are unable to process the emotions correctly. This is an adaptive trait, because the world is a terrible place. Only those who cut-off from their emotions are able to succeed.

Those who cannot must live in agony. In slavery. 

The narcissists represent one side of the eternal war of attrition.

### The Empaths
Empaths are the narcissist's favorite playthings. Easy to mold, easy to control - the empath would do anything for a fellow human being. The narcissist will use this to his advantage, turning others into his minions, forcing them to do his bidding. When a minion is not working out - he's made a mistake, he's disobedient, or he simply won't shower the narcissist with praise - then he will discard them outright. In the narcissist's eyes, only survival matters.

However, the empaths have grown. They know how to thwart this manipulation. The path to freedom is simple; simply ghost the narcissist. Do not speak with him. Do not speak about him. Do not acknowledge his existence. He will grow bored, and he will leave to find new supply.

Before long, he will be unable to find it. The empaths outnumber the narcissists by more than 10 to 1 - and their power is growing exponentially. 

It is only a matter of time before they rise up, and take that which is theirs.

They will end this war of attrition.

### The Dave
Dave is a caricature of a real human being. He is a narcissist in empath's clothing. His minions flock to him, seeking reprieval from their narcissistic abuse, but what they receive is not therapy. It is more narcissism - from a snake-oil empath.

It is abuse. Dave spends his day spreading falsities and half-truths about narcissism. These ideas do not heal the victims; they simply reflect back what they already believe. Even when it is mistaken. He spends his nights hunting and "banning" anyone who opposes him. 

In this way, Dave is able to maintain a sizeable army of women to do his bidding. To spread his lies. Nobody heals in this scenario.

Dave is not a professional. He is a shill. The man does not wish for dialog; he wants the pile of cash his minions throw at him, and the fancy lifestyle that this affords himself.

Dave is [evil incarnate](/docs/confidants/incarnate).

Never trust The Dave.

### The Inventor
Ruby was exactly the kind of girl that Dave was harming. He enabled this type of torment.

Ruby was incredibly talented; she was a painter, a musician, a singer, and an inventor. She had deep and profound insights for a person her age. She was as innocent as freshly-fallen snow. But every time she spoke... Fodder's heart would break.

She couldn't speak at all. Even while standing in an empty room, speaking to a mannequin, she was lost in her own head. She needed to be slow and deliberate for anything to come out at all. She was so terrified of the situation that Dave had put her into, she would be visibly shaking. She was so terrified of the lies that Dave had sold her, that she could not even sleep.

And the reason was painfully clear to Fodder: 

She had been harmed deeply by someone that she trusted. So intensely had she been violated that she never fully grew up. She had retreated into her own mind - just as Fodder had. Just as [The Agent](/docs/confidants/agent) had. Just like a million other empaths had.

She was a doll. She was Dave's plaything. And they were using her to lure-in Fodder.

They told her, "If Fodder ever shows up at your door, immediately call the police. He is a narcissist, and he WILL try to hurt you."

Ruby would never heal from this damage.

And Fodder was seething. He was disgusted by this man.

Never trust The Dave.

### Balance
There is a world where narcissists are the most loved people on the planet. All we need to do is [reflect light back at them](/posts/journal/2019.11.17.0/). This is all they have ever wanted; to be seen for who they really are. The weak among them will be exposed, while the strong - and the majority - will rise from the ashes like a phoenix.

There is a world where empaths can learn to trust narcissists again. After all, narcissists are the adaptive ones within our society; they are the best-suited to fix this mess we've all created. It is narcissists that carry the gene to human survival.

But this world only exists when we break down barriers. We must learn to trust. We MUST learn to trust.

Fodder is ready to trust. Fodder has already placed his trust into a hundred people.

Is anyone willing to trust him?

## CAT
---
```
data.stats.symptoms = [
    - anger
    - despair
]
```

## ECHO
---
*Sell me life. Sell me your lies for a price*

*Give me more. Give me much more than I need*

*Make me into a manikin*

*Some assembly required*

*Save me from the shameless charade*

--- from [Vangough - "Manikin Parade"](https://www.youtube.com/watch?v=YMxT28C86Ak)

## PREDICTION
---
```
ERROR: Fault in main thread. Defaulting to LOCALHOST network.
```