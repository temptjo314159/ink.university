---
author: "Luciferian Ink"
date: 2019-11-19
title: "A Lonely Town: Lancaster"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
[The Raven's](/docs/confidants/her) story.

## ECO
---
Lancaster is to be a test of Fodder's theory of [trust and verification](/posts/theories/verification/). Essentially, 3 or more real-world locations are to be turned into [Lonely Towns](/docs/scenes/lonely-towns). Each mayor is to operate independently of the next. If Fodder's theory of [Pillars](/docs/pillars/) is correct - and words actually shape reality - then each town should end up with a similar style - even though their creators have never met. 

The following cities are prime candidates:

- Lancaster, PA, USA
- Lancaster, TX, USA
- Lancaster, CA, USA
- Lancaster, UK
- Lancaster, AU (Required)

Fodder would need to visit three of these locations before the world ended.

Doing so would end the mind control.

### Theme
Lancaster is to be styled by The Raven and The Queen. Other locations are to by styled by their respective female mayors.

### Mission
Prove that women should rule the world.

Fodder is already convinced. Make the world understand.

### Other
Other areas should take inspiration from the first Lonely Town, GunsPoint.

## CAT
---
```
data.stats.symptoms = [
    - blissful delusion
]
```

## ECHO
---
*You will want to know where I am now you know my dear*

*I was in the cherry tree when mother said to me*

*"You are only seventeen, you don't know what it means"*

*So I climbed down and picked the cherries that fell to the ground*

*I can't carry it alone*

*So I'll bury it like bones*

*You are only seventeen you don't know what to fear*

*All we know is what we have to hold and call my dear*

*I will want to know where you are if you're far or near*

*I don't know how loud I'll have to sing for you to hear*

*If I can't marry you I'll go*

*On my chariot of gold*

*There is only this I know*

*I will die before I'm old*

--- from [Magnolian - "The Bride & the Bachelor"](https://www.youtube.com/watch?v=QblesJCcw8s)

## PREDICTION
---
```
The Raven is already making this town happen. The Queen is too - in her dreams.

The Agent and The Seer should also be considered for mayor.
```