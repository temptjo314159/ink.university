---
author: "Luciferian Ink"
date: 2007-04-20
title: "The Gang Starts a Band"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
[The Burninators perform TROGDOR](https://www.youtube.com/watch?v=drDDtyxP5zk)

## ECO
---
Heh. 420, man.

## CAT
---
```
data.stats.symptoms = [
    - nervousness
    - excitement
    - happiness
]
```

## ECHO
---
*Trogdor!*

*Trogdor!*

*Trogdor was a man*

*I mean, he was a dragon man*

*Or maybe he was just a dragon*

*But he was still Trogdor!*

*Trogdor!*

*Trogdor!*

*Burninating the countryside,*

*Burninating the peasants*

*Burninating all the peoples*

*And their thatched-roof cottages!*

*Thatched-roof cottages!*

*Whoa, this has wicked dueling guitar solos*

*It's like squeedly versus meedley over here*

*Go squeedly!*

*Go squeedly!*

*Squeedly wins!*

*When all the land is in ruins*

*And burnination has forsaken the countryside*

*Only one guy will remain*

*My money's on*

*Trogdor!*

*Trogdor!*

*And the Trogdor comes in the night...*

--- from [Strong Bad - "Trogdor!!"](https://www.youtube.com/watch?v=7gz1DIIxmEE)