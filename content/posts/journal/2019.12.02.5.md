---
author: "Luciferian Ink"
date: 2019-12-02
title: "The Convert"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## ECO
---
While in my Uber to the Hayworth, and back to the Paper Factory, I was pleased to make the acquaintance of an African man, who was more than willing to talk politics with me.

Or, rather, he wanted to complain about [The President](/docs/confidants/dave). He told me that he had voted for this man during the first election - and that he had converted. He would vote Democrat this time.

He said that he was tired of the lies. That the President had done nothing for him, even though he pretended that he had.

He was sure that The President would not be re-elected. Coming from Texas - I was not so confident. Nor was I sure this would be the best-case scenario. If my story is true, then the President may be the best one we've ever had.

(It's a long-shot, I know.)

The man was quite talkative, and I was happy to listen. We agreed on a lot of things, and I was happy to reflect his ideas back at him. I know exactly what it is like to feel like you aren't being heard. 

The man told me of his hardships. He told me about how expensive it was to live in NYC. He told me about the crippling debt, and his family's run-down apartment. He told me about how hard it was for someone like him to find good work. He told me about how racist he believes the police are. He told me everything.

The man just wanted to talk. And I wanted to make an impact on his life. So, I listened. And, where appropriate, I planted seeds:

- Perhaps capitalism is the problem. Wouldn't life be better if we all just took care of each other?
- Perhaps The President is actually BREAKING THE NEWS. Like, the 24-hour news cycle is clearly killing society. We need HISTORY - not news.
- I spoke about [The Robot](/docs/confidants/robot). I spoke about what automation was doing to society. About how big tech companies were not paying their fair share. About how we would all, eventually, be automated out of work.
- I told him a very sensitive secret about what I had been exposed to at [The Agency](/docs/candidates/the-machine). He was the first and last person I've ever told about this.

After about an hour in the car, we had arrived back at the Paper Factory.

"Here," I said to him. "Take my business card. It's my last one. I like the way you think."

"Okay!" he said. "I'll check it out."

"Also, what was the name of the artist you mentioned? You told me about some song lyrics earlier."

"Oh, it's an African artist. You probably wouldn't like it."

"Try me. I listen to a lot of different things."

"He's called Anonymous."

Curious. That's quite the coincidence. When The Agency [banned Identity](/posts/bans/ban.0/), they effectively made all of us anonymous. 

This guy knows what's up. 

## CAT
---
```
data.stats.symptoms = [
    - empathy
]
```

## ECHO
---
*Negativity is a powerful force*

*That causes you to divorce*

*The idea of success*

*Will make you settle for less*

*If they aren't helping you, they're hurting you*

*So you must remain true to who you are*

*And remain positive*

*And get rid of those who are negative*

--- from [Anonymous - "F.Y.O."](https://www.youtube.com/watch?v=el_m57HjaSs)