---
author: "Luciferian Ink"
date: 2018-03-18
title: "The End of Heartache"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
[The Queen's](/docs/confidants/mother) story.

## ECO
---
[Fodder](/docs/personas/fodder) would experience suicide three times over his lifetime. Each successive time would change him.

### The Hanging
When Fodder was in middle school, he would attend church with his family. After, they would retreat to the cafeteria, where lunch was served to the congregation each Sunday. At some point, Fodder would leave to play with the other kids. He would return to his parents after a short while.

Mother would turn to him, exclaiming, "Oh my gosh, Fodder, did you hear what happened? Kyle's mom killed herself."

Fodder felt like he had been punched in the stomach. He became nauseous. His head would start to spin. He began to sweat. He couldn't even formulate a response.

"They found her hanging in the garage this morning. Depression."

Fodder had no idea how to handle these feelings. He put on a brave face, pretending like it didn't affect him, and went on with life.

### The Fire
Shortly after Fodder purchased his first house, he would be notified of another suicide by his brother, [The Reverend](/docs/confidants/reverend). 

Their mutual friend from high school, Brad, had passed away just this morning. He had waited for his family to leave the house, set it on fire, then took his life with a gun.

It was depression. Again. Brad had lost his little sister just a couple of years previously, and in the wake, he was drowning. He had no job. He wasn't attending college. He was overweight, lonely, and all of his high school friends had moved-away. He spent every day playing video games.

Eventually, he gave up.

The Reverend was in tears, but Fodder maintained his emotionless persona. Truth be told, he barely felt anything. He was concerned about his brother, but regardless, he slept well that night.

### The Blade
Much later in life, The Queen would confide that she had witnessed her own mother, Fodder's grandmother, attempt suicide. Depression, again.

As a child, she had discovered a box while playing with her mother's things. Within, she would find a razor blade, but think nothing of it.

The following day, she would go into her mother's room, to say "goodbye" before heading-off to school. She would find her mother unresponsive. Upon pulling-back the covers, she would find her mother covered in blood, wrists wide-open. She would call the police. Her mother would survive the attempted suicide - though she would still die young. Lung cancer, from smoking cigarettes.

The Queen still wondered if she could have done something to prevent the attempt. If only she had realized what the razor she found would be used for.

Fodder would listen intently, and he would project empathy. 

But he would feel nothing at all.

## CAT
---
```
data.stats.symptoms = [
    - numbness
]
```

## ECHO
---
*You know me*

*You know me all too well*

*My only desire*

*To bridge our division*

--- from [Killswitch Engage - "The End of Heartache"](https://www.youtube.com/watch?v=JiDnB-CrrNs)