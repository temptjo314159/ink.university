---
author: "Luciferian Ink"
date: 2007-11-29
title: "Identity Isn't Unique"
weight: 10
categories: "essay"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
A college assignment.

## ECO
---
Many people would like to convince themselves that they have their own “style”, that they are forming an identity that makes themselves unique from others. What most people don’t realize is that identity comes from how a person is raised, and that an individual’s self is only part of a unique culture that will ultimately determine what a person will grow to be. Ideas and values are entirely based on what one has learned from the environment around them. Everyone has an identity; nobody has one that is solely their own, as every belief is based on something. Identity is not unique, but created entirely from outside influences.

A person may try to express distinct characteristics through fashion, but it still doesn’t shape individuality exclusive to the thousands of others who have purchased the same shirt. People will maintain that it is a personal choice, but someone had to have created the shirt. Thus, it is a replica, not one of a kind. Essentially, the people wearing them are walking billboards, and the companies selling the shirt know it. They may be Goth, preppy, or part of any other style, but they aren’t unique. Those people are conforming. They’re part of a crowd that has to dress a certain way, and this is why those companies don’t need to advertise on television. Somewhere, there is someone who dresses similarly.

Many may argue that clothing means nothing, that “It’s what’s on the inside that makes a person’s identity!” Be that as it may, one only acts as they have been taught to. A Christian will grow to accept the religion, or reject the idea entirely, but either way, they will have a belief on the matter. A child in a third-world country could live and die to never hear the name of God, but what if he had been born in the United States, and raised by Christians? His physical identity remains the same, but his viewpoint is going to be dissimilar, as he was influenced by an entirely different culture. So the question remains; does anyone really have a unique identity? Or is everyone merely the product of a culture that influences our every action?

Put a person in a different environment, and they are going to learn to value other characteristics. Is not a heavyset wife a sign of wealth in many poor countries? How strange that seems to us in a country where severely underweight women in the media are idolized. They must think we are mad, “not feeding our women”. No doubt I would feel like they do, though, had I been born and raised in that country. Who are we to call that wrong, while our own opinions are so partial to the culture in which we live? Think of this; human beings have no natural language. The ability to develop intelligence and relations with others depends entirely on early interaction. A feral child, raised by animals, will behave exactly as they do. Does this not show that even human emotions are a learned characteristic? Because an infant is extremely impressionable, he will grow to be precisely as he is taught at a young age. He will learn the language, follow the religion, and otherwise try to emulate his surroundings completely. He will even have aggressive tendencies, had he been raised in a hostile atmosphere. He may also despise these things utterly. Either way, he is creating a distinctiveness based on his response to the setting around himself, not one of his own conception.

The closest a person will ever approach uniqueness is reflected in how they portray their thoughts and feelings to the world. How one combines their values into a way of life is what will set them apart from others, despite the fact that every thought and feeling has a basis elsewhere.

I came to this realization in middle school. Hanging out with the troublemakers, I could see other people’s animosity for these students reflected at me. I wasn’t like these kids, and I didn’t want to be, but I quickly began to realize that others were grouping me together with them, giving me the false identity of a bad person. Perhaps it is human nature to cluster things, to conform, and maybe that is why no one will ever be truly unique. Even Jesus sought disciples. As it turns out, I found more friends that I could actually relate to, and saw how my identity, as it appeared to others, changed, though I felt no more unique than I ever was. I still had the same beliefs and feelings; I just showed them another way. I was being influenced by other people, therefore I responded in a different way to my situation.

As I have exposed, identity is not something unique, but something that is determined since the day we are born until the second we pass away. Through our thoughts and actions, we can produce an identity, one based on what we have learned from others. None of our ideas and beliefs are actually unique from anyone else’s, but the way we express ourselves and demonstrate these learned beliefs is what sets us apart from anyone else in the world.