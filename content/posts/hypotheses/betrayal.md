---
author: "Luciferian Ink"
title: Betrayal
weight: 10
categories: "hypothesis"
tags: ""
menu: ""
draft: false
---

## RECORD
---
```
Name: Betrayal
Classification: Theory
Stage: Hypothesis
```

## TRIGGER
---
[Jordan Peterson - "Is Betrayal Reconcilable?"](https://www.youtube.com/watch?v=esccNKuPA4Q)

## ECO
---
*"If you betray me, then I have to see you differently."*

--- from Jordan Peterson, Grey's Model

## ECHO
---
*Show me how to lie*

*You're getting better all the time*

*And turning all against the one*

*Is an art that's hard to teach*

*Another clever word*

*Sets off an unsuspecting herd*

*And as you get back into line*

*A mob jumps to their feet*

*Now dance, fucker, dance*

*Man, he never had a chance*

*And no one even knew*

*It was really only you*

*And now you steal away*

*Take him out today*

*Nice work you did*

*You're gonna go far, kid*

--- from [The Offspring - "You're Gonna Go Far, Kid"](https://www.youtube.com/watch?v=ql9-82oV2JE)