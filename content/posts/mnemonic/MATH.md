---
author: "Luciferian Ink"
title: "MATH"
weight: 10
categories: "mnemonic"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
- Donald Trump's "#MAGA"
- Andrew Yang's "#MATH"

## ECO
---
- **M**: Make
- **A**: Anarchists
- **T**: Think
- **H**: Harder

## CAT
---
```
data.stats.symptoms [
    - certainty
]
```