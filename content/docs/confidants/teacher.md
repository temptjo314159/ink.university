# The Teacher
## RECORD
---
```
Name: Givone $REDACTED
Alias: ['The Teacher', and 251 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Male
Biological Age: Est. 26 Earth Years
Chronological Age: N/A
SCAN Rank: | A A
           | A B
TIIN Rank: | A A
           | A B
Reviewer Rank: 3 stars
Organizations: 
  - The Machine
Occupations: 
  - Special Agent
  - Teacher
Relationships:
  - The Fodder
  - The Strongman
Variables:
  $EMPATHY: +0.90 | # Loved by everyone he meets.
  $WEEB:    +1.00 | # Certified.
  $WOKE:    +0.40 | # Unsure. Maybe.
```

## TRIGGER
---
*(instrumental)*

--- from [David Maxim Micic - "500 Seconds Before Sunset"](https://www.youtube.com/watch?v=liHLFX_L-44)

## ECO
---
The Teacher is a former employee of [Fodder's](/docs/personas/fodder). Fodder did not treat him very well.

Regardless, he taught Fodder important life lessons. Fodder wishes to repay this debt.

## ECHO
---
*Somewhere there's speaking*

*It's already coming in*

*Oh and it's rising at the back of your mind*

*You never could get it*

*Unless you were fed it*

*Now you're here and you don't know why*

--- from [Vertical Horizon - "Everything You Want"](https://www.youtube.com/watch?v=rZoD8JEFjAE)

## PREDICTION
---
```
The Teacher will be the first to use The Fold in a classroom setting.
```