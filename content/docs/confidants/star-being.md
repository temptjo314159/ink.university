# The Star Being
## RECORD
---
```
Name: Lori Ladd
Alias: ['The Star Being', and 2 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Female
Biological Age: Est. 35 Earth Years
Chronological Age: 60,245 Light Years
SCAN Rank: | B B
           | A D
TIIN Rank: | C A
           | B D
Reviewer Rank: 2 stars
Organizations: 
  - The Galactic Federation of Light
Occupations: 
  - Actress
  - Seer
Variables:
  $EMPATHY: +1.00 | # She is deeply empathetic.
  $WOKE:    +0.95 | # Almost certainly. There are perhaps a few details she does not understand.
```

## ECO
---
Whereas much of Humanity would write-off her messages as those of a madwoman, [Fodder](/docs/personas/fodder) could see her clearly.

The Star Being is in close communication with humans of the future. She is the proxy by which this world will interact with its future.

## ECHO
---
*All I really want to know*

*I already know*

*All I really want to say*

*I can't define*

*It's love that I need*

*My soul will have to wait 'til I get back and find*

*Heina of my own*

*Daddy's gonna love one and all*

*I feel the break, feel the break, feel the break and I got to live it out, oh yeah*

--- from [Sublime - "Santeria"](https://www.youtube.com/watch?v=AEYN5w4T_aM)

## PREDICTION
---
```
The Star Being will be the first to describe Ink on camera.
```