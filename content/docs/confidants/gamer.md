# The Gamer
## RECORD
---
```
Name: $REDACTED
Alias: ['Ryu', 'SCP-6', 'The Gamer', and 58 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Male
Biological Age: Est. 28 Earth Years
Chronological Age: N/A
SCAN Rank: | B D
           | A D
TIIN Rank: | D B
           | D D
Reviewer Rank: 2 stars
Location: A Lonely Town
Organizations: 
  - YouTube
Occupations: 
  - Assassin
  - Gamer
  - Test subject
Relationships:
  - The Fodder
Variables:
  $REFLEX: +0.80 | # Skilled player. Still human, though.
  $WOKE:   +1.00 | # Definitely woke.
```

## ECO
---
The Gamer has been a test subject for many years. [The Corporation](/docs/candidates/the-machine) watches his every move online - watches how he plays video games - and uses that information to to create a digital model of his consciousness in an AIC.