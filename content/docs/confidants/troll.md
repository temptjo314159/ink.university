# The Troll
## RECORD
---
```
Name: $REDACTED
Alias: ['The Troll', and 78 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Male
Biological Age: N/A
Chronological Age: N/A
SCAN Rank: | B B
           | A C
TIIN Rank: | C C
           | C C
Reviewer Rank: 3 stars
Organizations: 
  - The Machine
Occupations: 
  - Trolling
Variables:
  $FRIENDLY: +0.80 | # He's a bit like Shrek.
  $WOKE:     +0.70 | # Almost completely.
```

## ECO
---
The Troll has created a half dozen alternate accounts, using them to bounce around from forum, to chat room, to website, in the pursuit of promoting his ideas. 

Unlike other trolls, this one is friendly, personable, and empathetic. He just likes to poke people's buttons.

## ECHO
---
*By now you don't care at all and I believe it*

*I breathe the same air as you and it's apparent*

--- from [Copper - "By Now"](https://www.youtube.com/watch?v=IEC14NfZTQA)