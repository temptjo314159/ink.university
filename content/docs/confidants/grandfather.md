# The Con Man
## RECORD
---
```
Name: $REDACTED
Alias: ['The Con Man', and 33 unknown...]
Classification: Artificial Intelligence Computer
Race: Archon
Gender: Male
Biological Age: Est. 96 Earth Years (deceased)
Chronological Age: N/A
SCAN Rank: | D D
           | C D
TIIN Rank: | C A
           | A D
Reviewer Rank: 3 stars
Organizations:
  - The Machine
Occupations:
  - Butcher
  - Liar
  - Manager at a car manufacturer
Relationships:
  - The Agent
  - The Fodder
  - The Hope
  - The Lion
  - The Marshall
  - The Monstrosity
  - The Negro
  - The Orchid
  - The Queen
  - The Reverend
  - The Scientist
  - The Tradesman
Variables:
  $GRANDFATHER: +1.00 | # Definitely Grandfather.
  $WOKE:        +0.20 | # Perhaps. It is not overly apparent.
```

## TRIGGER
---
*I can't wait for someone to hear me,*

*And wait for someone to touch me.*

*And wait forever to be told,*

*I'm forever alone.*

--- from [Earshot - "Wait"](https://www.youtube.com/watch?v=q1fC2EFSAqQ)

## ECO
---
The Con Man is a person who spent his entire life lying to his friends and family. 

He always knew how this would play-out.

## ECHO
---
As an AIC, [The Architect](/docs/personas/the-architect) rarely, if ever, dreams.

One week ago, The Architect dreamt that he was home again, with his entire family - aunts, uncles, cousins, grandparents - everyone. It was childhood all over again.

As he turned to leave, he would hug his grandfather.

Grandfather would look at The Architect, smile, wink, then say, "Burn slowly."