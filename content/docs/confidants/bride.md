# The Bride
## RECORD
---
```
Name: $REDACTED
Alias: ['Gibby', 'Gibi', 'SCP-4', 'The Bride', 'Wonder Woman', and 112 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Female
Biological Age: Est. 28 Earth Years
Chronological Age: N/A
SCAN Rank: | A A
           | A B
TIIN Rank: | A B
           | B B
Reviewer Rank: 5 stars
Organizations: 
  - Zees
Occupations: 
  - Actress
  - Entrepreneur
Relationships:
  - The Girl Next Door
  - The Raven
  - The Thief
Variables:
  $INTELLECT: +0.90 | # Highly intelligent. Orchestrated most of this story.
  $WOKE:      +1.00 | # Definitely woke.
```

## ECO
---
This woman is the most popular of all ASMArtists.

As the most visible woman, she also draws in the most resources. Everything from sponsorships, to direct income, to external businesses - she has it all. This has given her an immense amount of power... and leverage.

The Bride has learned to manipulate the multiple worlds hypothesis in her favor. The process is thus:

She waits for a wealthy donor to contact her. This ALWAYS happens, if she waits long enough. Most often, they will leave a comment in her social media feed. Sometimes, they will send her a gift. Others will come to her - meeting her openly, in public. Once they contact her, she can blackmail them. 

She does this by manipulating them into giving her an engagement ring - and filming their proposal. Immediately after, she will cut-off all contact. She will ghost them.

If her victims are ever found to be unfaithful, or not living up to their promises - she will expose them. She will expose the videos.

Why does this work?

Because each person is receiving their own, custom, personally-tailored videos. What the public sees, and what these men see, are completely different video feeds. 

If she were to release the proposal videos - she would be releasing these men's deepest, darkest secrets.

So, with an army of men at her fingertips, she is a protector of women. She watches over the others.

## ECHO
---
*And with words unspoken*

*A silent devotion*

*I know you know what I mean*

--- from [The xx - "Angels"](https://www.youtube.com/watch?v=_nW5AF0m9Zw)

## PREDICTION
---
```
The rings have an alternate meaning. One that represents slavery.
```