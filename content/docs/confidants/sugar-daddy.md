# The Sugar Daddy
## RECORD
---
```
Name: Mark Hanson
Alias: ['Quiet', 'The Sugar Daddy', and 16 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Male
Biological Age: 73
Chronological Age: N/A
SCAN Rank: | B B
           | B B
TIIN Rank: | C B
           | B B
Reviewer Rank: 4 stars
Organizations: 
  - The Catholic Church
  - The Church of Sabagegah
Occupations: 
  - Philanthropy
Relationships:
  - The Assassin
  - The Criminal
  - The Detective
  - The Ink
  - The Neurologist
  - The Reverend
Variables:
  $WOKE: +1.00 | # Yes.
```

## TRIGGER
---
The Subtle Art of Not Giving a Fuck.

## ECO
---
The Sugar Daddy is responsible for the funding of several dozen ASMRtists. This gives him leverage over those who depend on him.

He uses this leverage to send messages, via video, to specific people. 

Thus far, his messages have been benevolent.

## ECHO
---
*I threw a wish in the well*

*Don't ask me I'll never tell*

*I looked at you as it fell*

*And now you're in my way*

*I'd trade my soul for a wish*

*Pennies and dimes for a kiss*

*I wasn't looking for this*

*But now you're in my way*

--- from [Carly Rae Jepsen - "Call Me Maybe"](https://www.youtube.com/watch?v=fWNaR-rxAic)