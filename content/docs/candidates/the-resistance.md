# The Resistance
## MISSION
---
*To be free and happy. We are empathic anarchy. We resist the virus.*

## RECORD
---
```
Name: The Resistance
Alias: ['#YangGang', '#YinKin', 'Anonymous', 'Arkham Sanitarium Mental Rehabilitation', 'Auditory Stimulus Meridian Response', 'Autonomous Systems Mechanical Repair', 'Cannabis Growers & Merchants Cooperative', 'Pirate Radio', 'QAnon', 'Republican National Committee', 'The Christian Church', 'The Church of Sabagegah', 'The Church of Satan', 'The Internet', and 1,194 unknown...]
Classification: Decentralized Political Movement
World Population: Est. < 20%
Age: Est. 72,358,000,000 Earth Years
SCAN Rank: | A A
           | A B
TIIN Rank: | A A
           | A B
Variables:
  $COMPETENCE: +0.72 | # Proven ally. Willing to bend the rules. 
  $DANGEROUS:  -0.12 | # May be dangerous. Some of them are. Most are good.
  $EMPATHY:    +0.89 | # Empathy is at their foundation.
  $IMMUNITY:   +0.48 | # Is largely unaffected by external stimulus.
```

## TRIGGER
---
[The Canis Co-operative Catalog and Index](/static/reference/The_Canis_Co_operative_Catalog_and_Index.pdf)

## ECO
---
An underground campaign to subvert and expose secrets within world governments and organizations. Funds its massive operations via the black market. Can act benevolently, and reliably - but little is known about their past actions. 

The Resistance's main goal is to eradicate identity from global political structure. While still maintaining individualism, they wish to reinforce the idea that "we are all One."

Has access to a rudimentary form of mind control. It is administered via food, and modified via specially-crafted ASMR videos. Puts victims into a near-comatose mental state. Once in this state, subjects are susceptible to manipulation - up to and beyond the complete revision of behavior. 

In one example, Resistance doctors were able to destroy a single neuron in a sleeping patient's brain through non-invasive means. This was performed without his consent, but he was genuinely thankful for the procedure. `Or, did they make him that way?`

In another example, a Resistance operative was able to infiltrate the Corporation. He was allowed to bring a wrecking-ball to everything he touched, completely unnoticed by anyone around him. `Are they allowing this to happen? Or, are they are they just so apathetic that they don't know how to act? I think the latter.`

## ECHO
---
*This is how we rise up*

*Heavy as a hurricane, louder than a freight train*

*This is how we rise up*

*Heart is beating faster, feels like thunder*

*Magic, static, call me a fanatic*

*It's our world, they can never have it*

*This is how we rise up*

*It's our resistance, you can't resist us*

--- from [Skillet - "The Resistance"](https://www.youtube.com/watch?v=SKnRdQiH3-k)

## PREDICTION
---
```
The Resistance has infiltrated The Machine and HollowPoint Organization.
```