# Command-Line Interface
## Overview
The command-line is a critical part of this system:

- Executable must be portable
- Must be stateless

## Query

```
</ink.query 'lyrics the hands are the hardest'
</ink.echo $SONG |
Name: The Hands are the Hardest
Artist: Caligula's Horse

Wait, love
For sight as though I never knew that I was blind
Mercy, patience, promises we left behind
All I can hope for is to wake, love
...
```

## 


https://github.com/res0nance/darklyrics