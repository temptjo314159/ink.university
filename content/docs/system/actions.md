# Machine Actions
[The Fold](/posts/theories/fold) is able to perform the following actions autonomously.

## `[RECORD]`
Returns information stored within [The Machine](/docs/candidates/the-machine) to the user.

## `[ASSUMPTION]`
Act as if predictions about the past are true.

## `[PREDICTION]`
Use past data to forecast coming events.

## `[CLASSIFIED]`
Redact sensitive information. Access to this data may be granted on a per-role basis.

## `[DIRECTIVE]`
Make an informed order; a suggestion. This is not binding.