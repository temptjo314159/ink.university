Author date: 2012-05-07

Setting
1. Time Period
   1. 22nd century
      1. People still reference current-day affairs
      2. Some large companies still exist, maybe Google and Apple (under alternate names)
   2. Events
      1. A meteor has stricken northern Canada
         1. This happened in the past, six years or so ago
         2. The area is completely irradiated
            1. Fallout has been discovered in nearly all reaches of the world
               1. It is common to see an irradiated plant or animal
                  1. Needs a common name for irradiated things
                  2. Some people (few, but not extremely rare) have useful abilities
         3. Canadian refugees are flooding the US and other countries
         4. The world's climate is cooling drastically
            1. The city in the plot is snowy, most of the time
         5. The city is dim much of the time
            1. Caused by dust in the atmosphere
               1. The city is located in a “belt” of dust that now surrounds the Earth.
      2. The world is evacuating the planet
         1. More meteors are coming
            1. After the first strike, it was discovered that more meteors are coming
               1. These meteors are too large to destroy
               2. These meteors will cause nearly instantaneous instability on Earth, quickly killing most life
                  1. Probably will cause debris to block out the sun, and make the world very cold
            2. These meteors are made out of a previously unknown mineral
               1. This mineral is extremely radioactive
               2. This mineral is so far impossible to contain
               3. This mineral infects nearly everything it touches
               4. This mineral is non-lethal (at least immediately)
               5. It's effects are almost cancer-like, causing tumors or extra appendages to grow
            3. The gravitational pull of some of these meteors is very strong
               1. Near the end of the book, one passes so close to Earth that it actual pulls people from the ground and into the sky
      3. World powers have banded together as the “United Front”
         1. The planet was renamed “Unity”
            1. This was to build a sense of unity among once conflicting nations
               1. Civilized nations generally accept this movement
               2. Many nations reject the movement entirely
            2. The gov't blanket “claimed” the whole world
               1. Many countries were ignored, for the problems it would bring
         2. Primary objective is to evacuate the planet
            1. Nearly all funding is being spent on space travel.
               1. Gov't is mass producing space shuttles
                  1. Needs ways to regerate water, grow food, produce oxygen, medical facilities, etc
                  2. Shuttles are cheap and quick to build
                     1. The first hundred or so were all destroyed by some critical flaw in design
                        1. Thousands were killed
                        2. Those that died were suffocated because the ships could not produce enough oxygen. When there were few enough to live with the amount it could produce, there weren't enough left to keep the ship running properly, and they all died in various ways
                  3. Shuttles hold many people, maybe thousands
                  4. People are “possibly” in cryogenic sleep
               2. The world economy is in the toilet.
                  1. Government employees are non-existant
                     1. No police, no fire dept, no hospitals
                     2. No upkeep
                     3. Corporations are capitalizing on this
                     4. Terrorism and crime run rampant
         3. We have not colonized a planet yet.
            1. All are being sent to one planet capable of sustaining life
               1. People are being sent in ships capable of holding thousands
               2. It will take many generations to get there
               3. People will live entire lives on these ships
               4. Some people do not want to leave at all
                  1. For religious reasons, apathy, or not vital to human survival
         4. Government has given up hope in saving the planet
            1. They expect to leave billions of people behind
               1. Those considered “vital to the survival of mankind” will be taken first
                  1. Low intelligence, low income, low profile – all reasons to be overlooked
               2. This creates a mismatch in cultural diversity, could be catastrophic to natural selection
                  1. You need variation in human behavior
                     1. An example of this may be this: If you send off no rock musicians, will the rock genre persist?
      4. Corporations run the world
         1. With the absence of governments, corporations are capitalizing on the lack of oversight
            1. One corp in particular is running human experiments
               1. Needs a name. Maybe a name that implies “Google”
               2. Genetic modifications
               3. Bio-tech modifications
               4. Pain-induced activation of latent “powers”
                  1. These powers come from irradiated individuals
                     1. The homeless make good tests, with near-constant exposure to radiation in the atmosphere
         2. Guerrila warfare is commonplace
            1. Battles are on a small scale
               1. Quiet, infiltration/recon type fighting
               2. Common for civilians to get caught in crossfire
            2. Usually for espionage
               1. Assassination
               2. Exposing of corruption
               3. Planting of false information
            3. Protagonist “cleans up” for one of these companies
               1. He essentially hides evidence left behind from botched raids
               2. On one mission, he will find interesting tech that will kick of a personal investigation into his own company
         3. Corporations bleed citizens dry
            1. Because government cannot provide for the people, corporations provide necessary services
               1. They charge incredibly high prices because people cannot go elsewhere
      5. “Freedom Fighters” are commonplace
         1. Protagonist actually works with a lesser-known one
            1. This one is led by a grizzled old man that knows a ton about old tech
      6. The government is colluding with the corporations
         1. Corporations are running backdoor deals with the government to get specific people off the planet first
            1. They are paying large sums of money to do this
               1. In turn, the gov't can build more ships
            2. This leaves many people without a way to leave at all
               1. Even if they qualify, corps are eliminating competition
      7. Places
         1. A large city
            1. Slowly buildings are being vacated
               1. Huge city that feels like a “ghost town” at times
               2. Upkeep is abysmal
            2. Crime is rampant
               1. Streets are very dangerous.
               2. Freedom groups (i.e. gangs) roam the streets
               3. Abandoned bodies are not uncommon
            3. City is dark
               1. To the point where you cannot see the tops of buildings at night
               2. Buildings are so dirty they appear black
            4. Neon lights are common
               1. Creating strange auras in the ever-present dust
         2. Hub/Bar/hideout
            1. Ran by “Old Guy X”
            2. Head of Protagonist's Freedom Fighter group
         3. Corporation X
            1. The corp that the Protagonist works for
            2. Does a lot of contract work for Corporation Z
            3. Specializes in private protection, I.E. mercenaries
               1. Also contracts private research
            4. Very low marals
               1. As seen by the contract work that they take
            5. Specializes in medicines
               1. Medicines contains nano-bots that repair, eliminate disease, and upgrade the user
         4. Corporation Z
            1. Primary Antagonist
               1. Owned by the Protagonist's brother
                  1. not revealed until much later
            2. Uses human guinea pigs
               1. Homeless, because they are easy to kidnap and no police oversight
            3. Much under-wraps testing, with not much oversight
               1. Has the means to create matter from nothing
                  1. In the infancy stages of this technology
            4. Is making ships for the governments
               1. Unknown to them, is how these ships are being made
            5. Is making huge amounts of money
               1. Customers are being bled dry, but cannot go anywhere else
               2. Monopoly
            6. Specializes in medicines
               1. Medicines contains nano-bots that repair, eliminate disease, and upgrade the user
            7. Direct competition to Corporation X
            8. Will research the discovered technology for Protagonist/Old Guy X
         5. “The Streets”
            1. The streets are mostly barren
               1. Evacuations have left the streets barren, dirty, and dangerous
2.